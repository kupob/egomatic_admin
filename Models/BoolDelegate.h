#ifndef BOOLDELEGATE_H
#define BOOLDELEGATE_H

#include <QStyledItemDelegate>
#include <QComboBox>

class BoolDelegate : public QStyledItemDelegate
{
public:
    BoolDelegate(QObject* parent=0);
    ~BoolDelegate();

    virtual QWidget* createEditor(QWidget* parent,
                                  const QStyleOptionViewItem& option,
                                  const QModelIndex& index) const;

    virtual void setEditorData(QWidget* editor,
                               const QModelIndex& index) const;

    virtual void setModelData(QWidget* editor,
                              QAbstractItemModel* model,
                              const QModelIndex& index) const;
};

#endif // BOOLDELEGATE_H
