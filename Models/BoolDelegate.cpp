#include "BoolDelegate.h"
#include <QComboBox>

BoolDelegate::BoolDelegate( QObject* parent)
    : QStyledItemDelegate(parent)
{
}

BoolDelegate::~BoolDelegate()
{

}

QWidget* BoolDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    QComboBox* cb = new QComboBox(parent);

    cb->addItem("Нет", false);
    cb->addItem("Да", true);
    return cb;
}

void BoolDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    if (QComboBox* cb = qobject_cast<QComboBox*>(editor)) {
        bool value = index.data(Qt::EditRole).toBool();
        int cbIndex = cb->findData(value);
        if (cbIndex >= 0)
            cb->setCurrentIndex(cbIndex);
    }
    else
    {
        QStyledItemDelegate::setEditorData(editor, index);
    }
}

void BoolDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{

    if (QComboBox* cb = qobject_cast<QComboBox*>(editor))
        model->setData(index, cb->currentData(), Qt::EditRole);
    else
        QStyledItemDelegate::setModelData(editor, model, index);
}

