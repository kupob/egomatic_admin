#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <Forms/ItemPage.h>

#include <QSizeGrip>
#include <QMouseEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent, Qt::FramelessWindowHint),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
//    ItemPage* itemPage = new ItemPage(this);
//    ui->stackedWidget->insertWidget(1, itemPage);

    ui->backButton->setEnabled(false);

    ui->buttonGroup->setId(ui->backButton,      0);
    ui->buttonGroup->setId(ui->drinksButton,    1);
    ui->buttonGroup->setId(ui->equipmentButton, 2);
    ui->buttonGroup->setId(ui->customersButton, 3);

    ui->buttonGroup->setId(ui->statsButton,     4);
    ui->buttonGroup->setId(ui->usersButton,     5);

    connect(ui->buttonGroup, SIGNAL(buttonClicked(int)), ui->stackedWidget, SLOT(setCurrentIndex(int)));
    connect(ui->buttonGroup, SIGNAL(buttonClicked(int)), this, SLOT(setHeader()));
    connect(ui->closeButton, SIGNAL(clicked()), this, SLOT(close()));

    QSizeGrip* sizeGrip = new QSizeGrip(this);
    sizeGrip->setObjectName("sizeGrip");
    sizeGrip->setContentsMargins(0, 0, 0, 0);

    ui->verticalLayout->addWidget(sizeGrip, 0, Qt::AlignBottom | Qt::AlignRight);

    ui->headerFrame->installEventFilter(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showAsAdmin(bool isAdmin)
{
    ui->drinksButton->setVisible(isAdmin);
    ui->equipmentButton->setVisible(isAdmin);
    ui->usersButton->setVisible(isAdmin);
}

void MainWindow::setHeader()
{
    QAbstractButton* currentPageButton = ui->buttonGroup->button(ui->stackedWidget->currentIndex());
    QString text = currentPageButton->text();

    if (ui->stackedWidget->currentIndex() == 0)
    {
        ui->backButton->setEnabled(false);
        text = QString("Главное меню");
    }
    else
        ui->backButton->setEnabled(true);

    ui->titleLabel->setText(text);
}

bool MainWindow::eventFilter(QObject* watched, QEvent* event)
{
    if( watched == ui->headerFrame )
    {
        switch( event->type() )
        {
            case QEvent::MouseButtonPress:
            {
                QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
                _prevMousePos = mouseEvent->pos();
                break;
            }
            case QEvent::MouseButtonRelease:
            {
                _prevMousePos = QPoint();
                break;
            }
            case QEvent::MouseMove:
            {
                if (!_prevMousePos.isNull())
                {
                    QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
                    QPoint curPos = pos();
                    QPoint newMousePos = mouseEvent->pos();
                    move(curPos + (newMousePos - _prevMousePos));
                }
                break;
            }
            default:
                break;
        }
    }

    return QMainWindow::eventFilter(watched, event);
}
