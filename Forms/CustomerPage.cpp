#include "CustomerPage.h"
#include "ui_ItemPage.h"
#include <Utils/DbAdapter.h>
#include <ORM/CustomerGateway.h>
#include <Models/BoolDelegate.h>

CustomerPage::CustomerPage(QWidget *parent) :
    ItemPage(parent)
{
    _model = QPointer<CustomerPageModel>(new CustomerPageModel(this));
    setModel(_model);
    ui->tableView->resizeColumnsToContents();

    BoolDelegate* delegate = new BoolDelegate(this);
    ui->tableView->setItemDelegateForColumn(4, delegate);
}

CustomerPage::~CustomerPage()
{

}

void CustomerPage::addItem()
{
    Customer customer;
    customer.customerId = DbAdapter::instance()->getUuid();
    _model->addItem(customer);
}

void CustomerPage::removeItem()
{
    _model->removeItem(currentIndex());
}

void CustomerPage::save()
{
    QList<Customer> items = _model->getItems();
    CustomerGateway customerGateway;

    bool ok = false;
    if (!items.isEmpty())
        ok = customerGateway.saveItems(items);

    if (ok)
        _model->resetData();
}

void CustomerPage::cancel()
{
    _model->resetData();
}
