#include "LoginDialog.h"
#include "ui_LoginDialog.h"

#include "../Utils/DbAdapter.h"
#include <QCryptographicHash>
#include <QMessageBox>
#include <QMouseEvent>

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent, Qt::FramelessWindowHint),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);

    connect(ui->loginButton, SIGNAL(clicked()), this, SLOT(checkAuth()));
    connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->closeButton, SIGNAL(clicked()), this, SLOT(close()));

    ui->headerFrame->installEventFilter(this);

    ui->usernameLineEdit->setText(_settings.value("LoginDialog/lastUserName").toString());
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

bool LoginDialog::eventFilter(QObject* watched, QEvent* event)
{
    if( watched == ui->headerFrame )
    {
        switch( event->type() )
        {
            case QEvent::MouseButtonPress:
            {
                QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
                _prevMousePos = mouseEvent->pos();
                break;
            }
            case QEvent::MouseButtonRelease:
            {
                _prevMousePos = QPoint();
                break;
            }
            case QEvent::MouseMove:
            {
                if (!_prevMousePos.isNull())
                {
                    QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
                    QPoint curPos = pos();
                    QPoint newMousePos = mouseEvent->pos();
                    move(curPos + (newMousePos - _prevMousePos));
                }
                break;
            }
            default:
                break;
        }
    }

    return QDialog::eventFilter(watched, event);
}

void LoginDialog::checkAuth()
{
    QString userName = ui->usernameLineEdit->text();
    QString password = ui->passwordLineEdit->text();
    QString passwordEncr = QCryptographicHash::hash(QString("egomatic").toUtf8() + password.toUtf8(), QCryptographicHash::Md5).toHex();

    DbAdapter *db = DbAdapter::instance();
    bool isAdmin = false;
    bool ok = db->tryLogin(userName, passwordEncr, isAdmin);

    if (ok)
    {
        emit authAsAdmin(isAdmin);
        emit authSuccess();
        _settings.setValue("LoginDialog/lastUserName", ui->usernameLineEdit->text());
        close();
    }
    else
    {
        emit authFailed();
        QMessageBox::warning(this, tr("Авторизация"), tr("Неверное имя пользователя или пароль"));
    }
}
