#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include <QSettings>

namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();

    bool eventFilter(QObject* watched, QEvent* event);

private slots:
    void checkAuth();

signals:
    void authSuccess();
    void authFailed();
    void authAsAdmin(bool isAdmin);

private:
    Ui::LoginDialog *ui;

    QPoint _prevMousePos;
    QSettings _settings;
};

#endif // LOGINDIALOG_H
