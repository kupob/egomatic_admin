#include "TubePage.h"
#include "ui_ItemPage.h"
#include <Utils/DbAdapter.h>
#include <ORM/TubeGateway.h>
#include <Models/DrinkComboboxDelegate.h>
#include <Models/BoolDelegate.h>

TubePage::TubePage(QWidget *parent) :
    ItemPage(parent)
{
    _model = QPointer<TubePageModel>(new TubePageModel(this));
    setModel(_model);
    ui->tableView->resizeColumnsToContents();

    DrinkComboboxDelegate* delegate = new DrinkComboboxDelegate(this);
    ui->tableView->setItemDelegateForColumn(1, delegate);

    BoolDelegate* boolDelegate = new BoolDelegate(this);
    ui->tableView->setItemDelegateForColumn(2, boolDelegate);
}

TubePage::~TubePage()
{

}

void TubePage::addItem()
{
    Tube tube;
    tube.tubeId = DbAdapter::instance()->getUuid();
    _model->addItem(tube);
}

void TubePage::removeItem()
{
    _model->removeItem(currentIndex());
}

void TubePage::save()
{
    QList<Tube> items = _model->getItems();
    TubeGateway gateway;

    bool ok = false;
    if (!items.isEmpty())
    {
        for (int i = 0; i < items.count(); i++)
        {
            if (items.at(i).tubeSettingsId.isEmpty())
                items[i].tubeSettingsId = DbAdapter::instance()->getUuid();
        }

        ok = gateway.saveItems(items);
    }

    if (ok)
        _model->resetData();
}

void TubePage::cancel()
{
    _model->resetData();
}
