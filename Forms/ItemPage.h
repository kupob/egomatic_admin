#ifndef ITEMPAGE_H
#define ITEMPAGE_H

#include <QWidget>
#include <QSortFilterProxyModel>

namespace Ui {
class ItemPage;
}

class ItemPage : public QWidget
{
    Q_OBJECT

public:
    explicit ItemPage(QWidget *parent = 0);
    ~ItemPage();

protected:
    void setModel(QAbstractTableModel* model);

    void showEvent(QShowEvent* event);
    QModelIndex currentIndex() const;

protected slots:
    virtual void addItem();
    virtual void removeItem();

    virtual void save();
    virtual void cancel();

protected:
    Ui::ItemPage *ui;

    QSortFilterProxyModel* _sortModel;
};

#endif // ITEMPAGE_H
