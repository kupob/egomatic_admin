#include "ItemPage.h"
#include "ui_ItemPage.h"

ItemPage::ItemPage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ItemPage)
{
    ui->setupUi(this);

    connect(ui->addButton, SIGNAL(clicked()), this, SLOT(addItem()));
    connect(ui->removeButton, SIGNAL(clicked()), this, SLOT(removeItem()));
    connect(ui->saveButton, SIGNAL(clicked()), this, SLOT(save()));
    connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));

    _sortModel = new QSortFilterProxyModel(this);
}

ItemPage::~ItemPage()
{
    delete ui;
}

void ItemPage::setModel(QAbstractTableModel *model)
{
    _sortModel->setDynamicSortFilter(false);
    _sortModel->setSourceModel(model);
    ui->tableView->setModel(_sortModel);
    _sortModel->sort(0, Qt::DescendingOrder);
}

void ItemPage::showEvent(QShowEvent *event)
{
    Q_UNUSED(event);
    cancel();
}

QModelIndex ItemPage::currentIndex() const
{
    QModelIndex current = ui->tableView->currentIndex();
    QModelIndex sourceIndex = _sortModel->mapToSource(current);
    return sourceIndex;
}

void ItemPage::addItem()
{

}

void ItemPage::removeItem()
{

}

void ItemPage::save()
{

}

void ItemPage::cancel()
{

}
