#include "UserPage.h"
#include "ui_ItemPage.h"
#include <Utils/DbAdapter.h>
#include <ORM/UserGateway.h>
#include <Models/BoolDelegate.h>

UserPage::UserPage(QWidget *parent) :
    ItemPage(parent)
{
    _model = QPointer<UserPageModel>(new UserPageModel(this));
    setModel(_model);
    ui->tableView->resizeColumnsToContents();

    BoolDelegate* delegate = new BoolDelegate(this);
    ui->tableView->setItemDelegateForColumn(3, delegate);
}

UserPage::~UserPage()
{

}

void UserPage::addItem()
{
    User user;
    user.userId = DbAdapter::instance()->getUuid();
    _model->addItem(user);
}

void UserPage::removeItem()
{
    _model->removeItem(currentIndex());
}

void UserPage::save()
{
    QList<User> items = _model->getItems();
    UserGateway userGateway;

    bool ok = false;
    if (!items.isEmpty())
        ok = userGateway.saveItems(items);

    if (ok)
        _model->resetData();
}

void UserPage::cancel()
{
    _model->resetData();
}
