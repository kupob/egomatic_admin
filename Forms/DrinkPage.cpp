#include "DrinkPage.h"
#include "ui_ItemPage.h"
#include <Utils/DbAdapter.h>
#include <ORM/DrinkGateway.h>

DrinkPage::DrinkPage(QWidget *parent) :
    ItemPage(parent)
{
    _model = QPointer<DrinksPageModel>(new DrinksPageModel(this));
    setModel(_model);
    ui->tableView->resizeColumnsToContents();
}

DrinkPage::~DrinkPage()
{

}

void DrinkPage::addItem()
{
    Drink drink;
    drink.drinkId = DbAdapter::instance()->getUuid();
    _model->addItem(drink);
}

void DrinkPage::removeItem()
{
    _model->removeItem(currentIndex());
}

void DrinkPage::save()
{
    QList<Drink> items = _model->getItems();
    DrinkGateway drinkGateway;

    bool ok = false;
    if (!items.isEmpty())
        ok = drinkGateway.saveItems(items);

    if (ok)
        _model->resetData();
}

void DrinkPage::cancel()
{
    _model->resetData();
}
